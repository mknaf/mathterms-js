# About
A math terms generator written completely in Javascript.

PDF output is produced with
[texlive.js](http://manuels.github.io/texlive.js/).
This in turn relies on [Promises](https://www.promisejs.org/), for which
[this implementation](https://github.com/stackp/promisejs) is used.

# Setup
Info taken from [this stackoverflow question](http://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules)
## Git version 1.9+
With Git version 1.9 or newer, you can clone the submodules while cloning
the original repository:

    git clone --recursive -j4 git@gitlab.com:mknaf/mathterms-js.git

## Other Git versions

    git clone git@gitlab.com:mknaf/mathterms-js.git
    cd mathterms-js.git
    git submodule update --init --recursive
