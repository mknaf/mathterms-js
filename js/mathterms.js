"use strict";

var mathterms = {
    "max_num": null,
    "max_chars": null,
    "max_exp": null,
    "max_memb": null
};

/*
var polyglot = new Polyglot();
polyglot.extend({
    "hello": "Hello"
});
//console.log(polyglot.locale());
*/

// fix main menu to page on passing
$('.main.menu').visibility({
    type: 'fixed'
});
$('.overlay').visibility({
    type: 'fixed',
    offset: 80
});

// lazy load images
$('.image').visibility({
    type: 'image',
    transition: 'vertical flip in',
    duration: 500
});

// show dropdown on hover
$('.main.menu  .ui.dropdown').dropdown({
    on: 'hover'
});


// Generates a single term member
mathterms.gen_term_memb = function() {

    // the term we will return
    var memb = {
        "num": 0,
        "alphas": []
    };

    // preceding number
    memb["num"] = Math.floor(mathterms["max_num"] * Math.random()) + 1;

    // read letters from input, allow only lowercase letters
    var lower = $("input#alphas").val().replace(/[^a-z]+/g, '');
    // pick a random amount of random letters
    var chars = [];
    for (var i = 0; i < Math.floor(mathterms["max_chars"] * Math.random()) + 1; i++) {
        chars.push(lower.charAt(Math.floor(Math.random() * lower.length)));
    }
    // don't allow duplicate letters
    chars = chars.filter(function(x, n, s) { return s.indexOf(x) == n });

    // generate an exponent for each letter and append them to list of alphas
    for (var i = 0; i < chars.length; i++) {
        var e = Math.floor(mathterms["max_exp"] * Math.random()) + 1;
        // don't allow an exponent of 1
        if (e <= 1) {
            e = 2;
        }
        memb["alphas"].push({
            "bas": chars[i],
            "exp": e
        });
    }

    // sort chars in alphabetic order
    memb["alphas"].sort(function(a, b) {
        if (a["bas"] < b["bas"]) { return -1; }
        if (a["bas"] > b["bas"]) { return 1; }
        return 0;
    });

    return memb;
};

// Generates a complete term
mathterms.gen_term = function() {
    var term = [];

    // make length of term a bit random
    var max_memb = Math.floor(mathterms["max_memb"] * Math.random()) + 1;
    if (max_memb <= 1) {
        max_memb = 2;
    }

    for (var i = 0; i < max_memb; i++) {
        term.push(mathterms.gen_term_memb());
    }
    return term;
};

// Generates a bunch of terms
mathterms.gen_terms = function() {
    var terms = [];
    for (var i = 0; i < mathterms["max_terms"]; i++) {
        terms.push(mathterms.gen_term());
    }
    return terms;
};

mathterms.multiply = function(t) {

    var alphas = {};
    var num = 0;

    // for all the members we have
    for (var i = 0; i < t.length; i++) {

        // for all the bases/chars in the current subterm
        for (var j = 0; j < t[i]["alphas"].length; j++) {
            var bas = t[i]["alphas"][j]["bas"];
            // count the bases and exponents in alphas
            if (bas in alphas) {
                alphas[bas] += t[i]["alphas"][j]["exp"];
            } else {
                alphas[bas] = t[i]["alphas"][j]["exp"];
            }
        }

        // multiply numbers
        if (num == 0) {
            num = t[i]["num"];
        } else {
            num = num * t[i]["num"];
        }
    }

    // turn alphas into an array
    alphas = $.map(alphas, function(v, i) { return {"bas": i, "exp": v} });
    // sort the bases by alphabetical value
    alphas.sort(function(a, b) {
        if (a["bas"] < b["bas"]) { return -1; }
        if (a["bas"] > b["bas"]) { return 1; }
        return 0;
    });

    var m = {
        "num": num,
        "alphas": alphas
    };
    return m;
};

mathterms.memb2tex = function(t) {

    var alphas = "";
    for (var i = 0; i < t["alphas"].length; i++) {
        alphas += t["alphas"][i]["bas"] + "^{" + t["alphas"][i]["exp"] + "}";
    }

    // don't print a preceding number of value 1
    if (t["num"] <= 1) {
        var tex = alphas;
    } else {
        var tex = t["num"].toString() + alphas;
    }
    return tex;
}
	// one term into tex
mathterms.term2tex = function(t, solve) {

    var tex = [];
    for (var i = 0; i < t.length; i++) {
        tex.push(mathterms.memb2tex(t[i]));
    }
    tex = tex.join(" \\times ");

    if (solve == true) {
        tex += " &= " + mathterms.memb2tex(mathterms.multiply(t));
    } else {
        tex += " &= ";
    }
    return tex;
}

// Generates a preview from the currently set options
mathterms.preview = function() {

    $("#preview_unsolved").addClass("loading");
    $("#preview_solved").addClass("loading");

    $("button#generate_preview").addClass("disabled");
    $("#generate_pdf").addClass("disabled");
    $("#download_latex").addClass("disabled");

    var terms = mathterms.gen_terms();

    var tex = [];
    for (var i = 0; i < terms.length; i++) {
        tex.push(mathterms.term2tex(terms[i], false));
    }
    tex = "\\begin{align*}\n\t" + tex.join("\\\\[1em]\n\t") +  "\n\\end{align*}\n";
    $("#preview_unsolved").html(tex);
    $("#latex_unsolved").text(tex);

    tex = [];    for (var i = 0; i < terms.length; i++) {
        tex.push(mathterms.term2tex(terms[i], true));
    }
    tex = "\\begin{align*}\n\t" + tex.join("\\\\[1em]\n\t") +  "\n\\end{align*}\n";
    $("#preview_solved").html(tex);
    $("#latex_solved").text(tex);

    // update preview
    MathJax.Hub.Queue([
        "Typeset",
        MathJax.Hub,
        function() {  // callback
            $("#preview_unsolved").removeClass("loading");
            $("#preview_solved").removeClass("loading");

            $("button#generate_preview").removeClass("disabled");
            $("#generate_pdf").removeClass("disabled");
            $("#download_latex").removeClass("disabled");
        }
    ]);
};

mathterms.generate_latex_document = function() {
    var latex_solved = $("#latex_solved").text();
    var latex_unsolved = $("#latex_unsolved").text();

    // don't run if there are no terms, because texlive.js will
    // fail to compile the document
    if (latex_solved == "" || latex_unsolved == "") {
        return false;
    }

    var latex = `
\\documentclass[12pt]{article}
\\usepackage{amsmath}

\\begin{document}\n
` + latex_unsolved + "\n\\clearpage\n\n"
  + latex_solved
  + `
\\end{document}
`;

    return latex;
};

mathterms.generate_pdf = function() {
    var pdftex = new PDFTeX();

    var latex = mathterms.generate_latex_document();

    var myPopup = window.open("wait.html", "", "");
    function compile_pdf(myPopup) {
        pdftex.compile(latex).then(function(pdf) {
            myPopup.location = pdf;
        });
    }
    compile_pdf(myPopup);
};

mathterms.download_latex = function() {
    var latex = mathterms.generate_latex_document();

    var myPopup = window.open("", "", "");
    myPopup.location = "data:," + escape(latex);
};


$("#max_num").range({
    min: 1,
    max: 100,
    start: 3,
    onChange: function(value) {
        $('#display_max_num').html(value);
        mathterms["max_num"] = value;
    }
});
$("#max_chars").range({
    min: 1,
    max: 10,
    start: 3,
    onChange: function(value) {
        $('#display_max_chars').html(value);
        mathterms["max_chars"] = value;
    }
});
$("#max_exp").range({
    min: 2,
    max: 100,
    start: 3,
    onChange: function(value) {
        $('#display_max_exp').html(value);
        mathterms["max_exp"] = value;
    }
});
$("#max_memb").range({
    min: 2,
    max: 10,
    start: 3,
    onChange: function(value) {
        $('#display_max_memb').html(value);
        mathterms["max_memb"] = value;
    }
});
$("#max_terms").range({
    min: 1,
    max: 10,
    start: 3,
    onChange: function(value) {
        $('#display_max_terms').html(value);
        mathterms["max_terms"] = value;
    }
});

$("#chars_1").click(function() {
    $("#alphas").val("abc");
});
$("#chars_2").click(function() {
    $("#alphas").val("abcdefghijklmnopqrstuvwxyz");
});
$("#chars_3").click(function() {
    $("#alphas").val("xyz");
});


$("button#generate_preview").click(function() {
    mathterms.preview();
});

$("button#generate_pdf").click(function() {
    mathterms.generate_pdf();
});

$("button#download_latex").click(function() {
    mathterms.download_latex();
});
